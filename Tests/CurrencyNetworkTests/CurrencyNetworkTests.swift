import XCTest
@testable import CurrencyNetwork

final class CurrencyNetworkTests: XCTestCase {
    
    func testDecoder() {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "currencies", withExtension: "json") else {
            XCTFail("Missing file: currencies.json")
            return
        }
        guard let json = try? Data(contentsOf: url) else {
            XCTFail("Parser Error: cannot get data from json")
            return
        }
        
        guard let decoded = try? JSONDecoder().decode(CurrencyResponse.self, from: json) else {
              XCTFail("Parser Error: cannot decode json")
            return
        }
        XCTAssertNotNil(decoded, "Parser Error: cannot decode json")
        let listRates = decoded.currencies.sorted(by: {$0.country < $1.country})
        XCTAssertTrue (!listRates.isEmpty, "Parsing error: list is empty")
        XCTAssertEqual(listRates.first?.country,  "Argentina")

    }
    
    func testService() {
        
        let url = URL(string: "https://www.westpac.com.au")
        let network = NetworkService(baseURL: url)
        let service = CurrencyService.init(network: network)
        let expextation = expectation(description: "get currencies")
        service.getCurrencies{ (result) in
            
            switch result{
            case .success(let response):
                let listRates = response.currencies.sorted(by: {$0.country < $1.country})
                 XCTAssertNotEqual(listRates.first?.country,  "Not correct country")
                 XCTAssertEqual(listRates.first?.country,  "Argentina")
            case .failure(let error):
                 XCTFail("Parser Error: cannot decode json \(error)")
            }
            expextation.fulfill()
        }
        wait(for: [expextation], timeout: 30)
    }

    static var allTests = [
        ("testExample", testDecoder, testService),
    ]
}
