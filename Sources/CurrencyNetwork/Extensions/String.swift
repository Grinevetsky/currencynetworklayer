//
//  String.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 31/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation

extension String {
    /// Returns the date value of the String based on the formats determined by the API
    public func toDate() -> Date? {
        
        if let date = DateFormatter.dateFormatter.date(from: self) {
            return date
        }else if let date = DateFormatter.extendedDateFormatter.date(from: self) {
            return date
        }else if let date = DateFormatter.timeAndDateFormatter.date(from: self) {
            return date
        }else if let date = DateFormatter.dateAndTimeFormatter.date(from: self) {
            return date
        }
        return nil
    }
    
}
